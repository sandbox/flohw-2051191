<?php
/**
 * @file
 * Constants used in sthe module
 *
 * All constants used for chuck_approved module
 */

  define('CHUCK_APPROVED_FIELD', 'field_chuck_approved');
  define('CHUCK_APPROVED_ACTIVE', 'chuck_approved_active');
  define('CHUCK_APPROVED_TOKEN_URL', '[user:chuck_approved_url]');
  define('CHUCK_APPROVED_LIST_FORMS', 'chuck_approved_list_forms');
  define('CHUCK_APPROVED_ENABLE_FORMS', 'chuck_approved_enable_forms');
