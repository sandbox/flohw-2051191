This module can be used to prevent spamming. When installed, activate the module
in accounts settings and add the token [user:chuck_approved_url] in the mail
sent to user when his account is enabled.
You have to add two $conf variables :
  - $conf['recaptcha_private_key'] = '<your recaptcha private key>'
  - $conf['recaptcha_public_key'] = '<your recaptcha public key>'

You can also choose forms on which the module is activate. Just enable in
module's configuration the "Enable event activation messages on forms" option 
and click on links provided in messages when forms appears. Then the module will 
be activated on this form. When a user don't have an approved account and one of
the selected form will be displayed, the module will delete it to disallow user 
to post. Only administrator or user with ID 1 are allowed to bypass this module.

There is four permissions : one to allow users to list all forms on which
approval is required, a second to allow users to enable the events on form
(display messages to add form to the list), a third permission to allow users to
add forms to the list and a last permission to allow users to remove form from
the list

Here are the two routes used by the module :
  - approve-my-account/% : the parameter is a token to check if the account
      exists
  - approve-my-account-email : resend an email to the logged in user with a link
    allowing him to approve his account

In the account settings page (admin/config/people/accounts), a check box is
added under "Administrator role". Turn it on to enable the module. The token
[user:chuck_approved_url] is required in the two welcome mails.
On the edit user form, the check box to mark user as approved is deleted when
the editor is not administrator or user ID is 1.
