<?php
/**
 * @file
 * Admin functions
 *
 * All admin functions to manage activated forms.
 */

require_once drupal_get_path('module', 'chuck_approved') . '/chuck_approved.defines.php';

/**
 * Form to list all form_id on which module is enabled.
 *
 * @param array $form
 *   Drupal form
 * @param array $form_state
 *   Drupal form state
 *
 * @return array
 *   Form listing all activated form
 */
function chuck_approved_admin_list($form, &$form_state) {
  $list = chuck_approved_get_form_list();
  $content = array();
  foreach ($list->data as $fid) {
    $content[] = array(
      'title' => $fid,
      'actions' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t("delete"),
          '#href' => 'admin/config/workflow/chuck/' . $fid . '/delete',
          '#options' => array('query' => array('destination' => current_path())),
        ),
      ),
    );
  }
  $a = array();
  $form = chuck_approved_admin_enable_event(array(), $a);
  $form['chuck_approved_forms'] = array(
    '#type' => 'tableselect',
    '#js_select' => FALSE,
    '#header' => array(
      'title' => t("Form ID"),
      'actions' => t("Actions"),
    ),
    '#options' => $content,
    '#empty' => t("No forms saved, enable form events to add forms to the list."),
  );

  return $form;
}

/**
 * Form to enable events on forms to add it to the list.
 *
 * @param array $form
 *   Drupal form
 * @param array $form_state
 *   Drupal form state
 *
 * @return array
 *   Subform allowing to activate massages to activate chuck on form
 */
function chuck_approved_admin_enable_event($form, &$form_state) {
  if (user_access('enable chuck approved on forms')) {
    $form['chuck_approved_enable'] = array(
      '#type' => 'fieldset',
      '#title' => t("Event activation"),
      'chuck_approved_check' => array(
        '#type' => 'checkbox',
        '#title' => t("Enable event activation messages on forms"),
        '#description' => t("If checked, there will be a message on each form containing a link to activate events for the form. Only visible for your currently logged in user account."),
        '#default_value' => variable_get(CHUCK_APPROVED_ENABLE_FORMS, FALSE),
      ),
    );
    $form['actions'] = array(
      '#type' => 'actions',
      'submit' => array(
        '#type' => 'submit',
        '#value' => t("Save"),
      ),
    );
    $form['#submit'][] = 'chuck_approved_admin_enable_event_submit';
  }
  return $form;
}

/**
 * Treat the chuck_approved_admin_enable_event form.
 *
 * @param array $form
 *   Drupal form
 * @param array $form_state
 *   Drupal form state
 */
function chuck_approved_admin_enable_event_submit($form, &$form_state) {
  if ($form_state['values']['chuck_approved_check'] === "1") {
    variable_set(CHUCK_APPROVED_ENABLE_FORMS, TRUE);
  }
  else {
    variable_set(CHUCK_APPROVED_ENABLE_FORMS, FALSE);
  }
  drupal_set_message(t("Configuration saved"));
}

/**
 * Confirm to add the form to the approval requirements list.
 *
 * @param array $form
 *   Drupal form
 * @param array $form_state
 *   Drupal form state
 * @param string $form_id
 *   Drupal form id
 *
 * @return array
 *   Drupal confirm form
 */
function chuck_approved_admin_form_add($form, &$form_state, $form_id) {
  return confirm_form($form, t("Are you sure you want to add @form_id to the <em>chuck approved</em> list?", array('@form_id' => $form_id)), drupal_get_destination()
  );
}

/**
 * Treat the chuck_approved_admin_form_add form and add the form to the list.
 *
 * @param array $form
 *   Drupal form
 * @param array $form_state
 *   Drupal form state
 */
function chuck_approved_admin_form_add_submit($form, &$form_state) {
  $list = chuck_approved_get_form_list();
  $list->data[] = $form_state['build_info']['args'][0];
  drupal_set_message(t("Approval required on @form_id", array('@form_id' => $form_state['build_info']['args'][0])));
  variable_set(CHUCK_APPROVED_LIST_FORMS, $list->data);
  cache_set(CHUCK_APPROVED_LIST_FORMS, $list->data);
  drupal_goto('admin/config/workflow/chuck');
}

/**
 * Confirm to delete the form of the approval requirements list.
 *
 * @param array $form
 *   Drupal form
 * @param array $form_state
 *   Drupal form state
 * @param string $form_id
 *   Drupal form id
 *
 * @return array
 *   Drupal confirm form
 */
function chuck_approved_admin_form_delete($form, &$form_state, $form_id) {
  return confirm_form($form, t("Are you sure you want to add @form_id to the <em>chuck approved</em> list?", array('@form_id' => $form_id)), drupal_get_destination()
  );
}

/**
 * Delete the form from the list.
 *
 * @param array $form
 *   Drupal form
 * @param array $form_state
 *   Drupal form state
 */
function chuck_approved_admin_form_delete_submit($form, &$form_state) {
  if (($list = cache_get(CHUCK_APPROVED_LIST_FORMS)) == FALSE) {
    $list = (object) array('data' => variable_get(CHUCK_APPROVED_LIST_FORMS, array()));
  }

  if (($k = array_search($form_state['build_info']['args'][0], $list->data)) !== FALSE) {
    drupal_set_message(t("Approval no more required on @form_id", array('@form_id' => $form_state['build_info']['args'][0])));
    unset($list->data[$k]);
  }
  variable_set(CHUCK_APPROVED_LIST_FORMS, $list->data);
  cache_set(CHUCK_APPROVED_LIST_FORMS, $list->data);
  drupal_goto('admin/config/workflow/chuck');
}
